const gulp          = require('gulp');
const config        = require('../config.js');
const npmDist       = require('gulp-npm-dist');
const rename        = require('gulp-rename');
const clean         = require('gulp-clean');
const del           = require('del');
const runSequence   = require('run-sequence');

gulp.task('install:clean', function() {
    del([
        config.dest.root,
        config.src.img + '/**/*.*',
        config.src.svgIcons + '/**/*.*',
        config.src.pngIcons + '/**/*.*',
        config.src.sassGen + '/**/*.*',
        config.src.vendors + '/**/*.*',

        config.src.sass + '/pages/**/*.*',
        config.src.sass + '/components/**/*.*',
        config.src.sass + '/layout/**/*.*',
        config.src.sass + '/vendors/**/*.*',
        '!' + config.src.sass + '/components/_grid.*',
        '!' + config.src.sass + '/layout/_header.*',
        '!' + config.src.sass + '/layout/_footer.*',
        '!' + config.src.sass + '/layout/_layouts.*',
        '!' + config.src.sass + '/vendors/_import.*',

        config.src.templates + '/**/*.*',
        '!' + config.src.templates + '/_mixins.*',
        '!' + config.src.templates + '/home.*',
        '!' + config.src.templates + '/index.*',
        '!' + config.src.templates + '/data/_pages.*',
        '!' + config.src.templates + '/layouts/_layout.*',
        '!' + config.src.templates + '/partials/_header.*',
        '!' + config.src.templates + '/partials/_footer.*',
    ]);
});

gulp.task('install:vendors', function() {
    gulp.src(npmDist(), {base: config.root + 'node_modules/'})
        .pipe(rename(function(path) {
            path.dirname = path.dirname.replace(/\/dist/, '').replace(/\\dist/, '').replace(/\/build/, '').replace(/\\build/, '');
        }))
        .pipe(gulp.dest(config.src.vendors));
});

gulp.task('install', function() {
    runSequence(
        'install:clean',
        'install:vendors'
    );
});
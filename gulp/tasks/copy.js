const gulp   = require('gulp');
const config = require('../config.js');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const imagemin = require('gulp-imagemin');
const svgo = require('gulp-svgo');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');
const rename = require("gulp-rename");

gulp.task('copy:temp', function() {
    gulp
        .src([
            config.src.temp + '/**/*.*',
            '!' + config.src.temp + '/**/*.{jpg,jpeg,png,gif}'
        ])
        .pipe(gulp.dest(config.dest.temp));
    return gulp
        .src(config.src.temp + '/**/*.{jpg,jpeg,png,gif}')
        .pipe(imagemin([
            imagemin.gifsicle(),
            imageminJpegRecompress({
                min: 40,
                max: 60,
                quality: 'high'
            }),
            imagemin.optipng(),
            imagemin.svgo()
        ]))
        .pipe(gulp.dest(config.dest.temp));
});

gulp.task('copy:fonts', function() {
    return gulp
        .src(config.src.fonts + '/**/*.{ttf,eot,woff,woff2}')
        .pipe(rename(function (path) {
            path.basename = path.dirname;
        }))
        .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('copy:js', function() {
    return gulp
        .src(config.src.js + '/**/*.*')
        .pipe(gulp.dest(config.dest.js));
});

gulp.task('copy:libs', function() {
    gulp.src([
        config.src.vendors + '/jquery/jquery.min.js',
        config.src.vendors + '/**/*.js',
        '!' + config.src.vendors + '/tippy.js/tippy.min.js',
        '!' + config.src.vendors + '/tippy.js/tippy.standalone.min.js',
        '!' + config.src.vendors + '/jquery/core.js',
        '!' + config.src.vendors + '/jquery/jquery.slim.min.js'
    ])
        .pipe(concat('vendors.min.js'))
        .pipe(minify({ ext: {min: '.js'}, noSource: true}))
        .pipe(gulp.dest(config.dest.js));
});

gulp.task('copy:img', function() {
    return gulp
        .src([
            config.src.img + '/**/*.{jpg,png,jpeg,svg,gif}',
            '!' + config.src.img + '/svgo/**/*.*'
        ])
        .pipe(svgo())
        .pipe(gulp.dest(config.dest.img));
});

gulp.task('copy', [
    'copy:img',
    'copy:fonts',
    'copy:js',
    'copy:libs',
    'copy:temp'
]);

gulp.task('copy:watch', function() {
    gulp.watch(config.src.img + '/**/*.*', ['copy:img']);
    gulp.watch(config.src.js + '/**/*.*', ['copy:js']);
    gulp.watch(config.src.libs + '/**/*.*', ['copy:libs']);
    gulp.watch(config.src.temp + '/**/*.*', ['copy:temp']);
});

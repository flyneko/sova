$(function () {
    $('.js-dropdown').dropdown();
    $('.js-swipe-sidebar').swipeMenu().each(function () {
        var $this = $(this);
        var slideout = $this.data('slideout');
        var text = $this.text();
        slideout.on('beforeopen', function() { $this.text('Закрыть').css('position','absolute').css('top', ($(window).scrollTop() + (window.innerHeight / 2)) + 'px') });
        slideout.on('beforeclose', function() { $this.text(text); });
        slideout.on('close', function() { $this.attr('style', '').prop('style', ''); });
    });

    (function () {
        var $swipe = $('.js-swipe-nav').swipeMenu();
        var slideout = $swipe.data('slideout');
        var $swipeBtn = $('.swipe-button');
        slideout.on('beforeopen', function() { $swipeBtn.css('z-index', 9).css('position','absolute').css('top', ($(window).scrollTop() + (window.innerHeight / 2)) + 'px') });
        slideout.on('close', function() { $swipeBtn.attr('style', '').prop('style', ''); });
        $('.js-swipe-nav-close').click(function () { slideout.close(); });
    })();

    (function () {
        var $sidebars = $('.js-sidebar');
        var maxHeight = 0;
        var calc = function () {
            $sidebars.each(function () {
                if (this.offsetHeight > maxHeight)
                    maxHeight = this.offsetHeight;
            });
            $sidebars.css('min-height', maxHeight);
        };
        calc();
        $(window).resize(calc);
    })()

    $('.js-toggle').each(function () {
        new Toggle(this);
    });

    (function () {
        var init = function () {
            $('.js-replace').each(function () {
                var $target = $(this);
                var placeSelector = $target.data('place');
                var parent = $target.data('parent');
                var placeWidth = parseInt($target.data('width'));
                var $place = parent ? $target.parents(parent).find(placeSelector) : $(placeSelector);

                if (window.innerWidth <= placeWidth) {
                    $place.show()
                    if (!$place.data('old-place'))
                        $place.data('old-place', $target.wrap('<div>').parent());
                    $target.appendTo($place);
                } else {
                    $place.hide();
                    if ($place.data('old-place'))
                        $target.appendTo($place.data('old-place'));

                }
            });
        }

        init();
        $(window).resize(init);
    })();

    tippy('[title]');
});